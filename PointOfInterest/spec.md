Entidad: PointOfInterest  
===========================
  

[Licencia abierta](https://github.com/smart-data-models/dataModel.PointOfInterest/blob/master/PointOfInterest/LICENSE.md)

Descripción global: **Esta entidad contiene una descripción geográfica armonizada de un Punto de Interés** 

Utilizar los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: Tipo de entidad NGSI. Tiene que ser PointOfInterest  

##### Atributos descriptivos de la entidad
- `address`: La dirección postal 
- `category`: Categorí­a de este punto de interés. Valores permitidos: Los definidos por la [Taxonomía factual](https://github.com/Factual/places/blob/master/categories/factual_taxonomy.json) junto con las categorías extendidas descritas por la especificación. Por ejemplo, el valor `113` corresponde a playas, y el valor `311` corresponde a museos.  
- `dataProvider`: Una secuencia de caracteres que identifica al proveedor de la entidad de datos armonizada.   
- `description`: Una descripción de este artículo 
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon  
- `name`: El nombre de este artí­culo.  
- `owner`: Una lista que contiene una secuencia de caracteres codificada en JSON que hace referencia a los identificadores únicos de los propietarios
- `project`: Proyecto al que pertenece.
  
##### Atributos de la medición
- `Bind_NO`: Número que identifica al bind
- `Bind_NO2`: Número que identifica al bind
- `Bind_NOX`: Número que identifica al bind
- `Bind_PM1`: Número que identifica al bind
- `Bind_PM10`: Número que identifica al bind
- `Bind_PM25`: Número que identifica al bind

Propiedades requeridas  
- `category`  
- `id`  
- `name`  
- `type`  
