Entidad: PointOfInterest  
===========================
  

[Licencia abierta](https://github.com/smart-data-models/dataModel.PointOfInterest/blob/master/PointOfInterest/LICENSE.md)

Descripción global: **Esta entidad contiene una descripción geográfica armonizada de un Punto de Interés** 

Estas entidades son las que van del A01_XXX a la A07_XXX y las dos entidades W01_XXX y W02_XXX.

## Lista de propiedades  

##### Atributos de la entidad context broker
- `id`: Identificador único de la entidad  
- `type`: Tipo de entidad NGSI. Tiene que ser PointOfInterest  

##### Atributos descriptivos de la entidad
- `Category`: Categorí­a de este punto de interés. Valores permitidos: Los definidos por la [Taxonomía factual](https://github.com/Factual/places/blob/master/categories/factual_taxonomy.json) junto con las categorías extendidas descritas por la especificación. Por ejemplo, el valor `113` corresponde a playas, y el valor `311` corresponde a museos.  
- `DateCreated`: Fecha de creación de la entidad.   
- `Description`: Una descripción de este artículo 
- `Location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon  
- `Name`: El nombre de este artí­culo. 
  
##### Atributos de la medición
- `Bind_NO2`: Número que identifica al bind
- `Bind_O3`: Número que identifica al bind
- `Bind_PM10`: Número que identifica al bind
- `Bind_PM25`: Número que identifica al bind
- `Bind_SO2`: Número que identifica al bind

Propiedades requeridas  
- `id`  
- `Name`  
- `type`  
