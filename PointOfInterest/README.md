Última Especificación
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/pointofinterest/-/blob/main/PointOfInterest/spec.md) que se tiene. Contiene las entidades de los puntos de interés.

Otras especificaciones históricas
===
Entidades antiguas [Especificación antigua](https://gitlab.com/vlci-public/models-dades/pointofinterest/-/blob/main/PointOfInterest/archive/spec.md)
