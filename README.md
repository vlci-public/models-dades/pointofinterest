# pointOfInterest

Models de dades del projecte VLCi basats en els data models Fiware: https://github.com/smart-data-models/dataModel.PointOfInterest

### List of data models

The following entity types are available:
- [PointOfInterest](https://gitlab.com/vlci-public/models-dades/pointofinterest/-/blob/main/PointOfInterest/README.md). This entity contains a harmonised geographic description of a Point of Interest.
